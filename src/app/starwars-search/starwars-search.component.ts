import { Component, OnInit } from '@angular/core';
import {SearchService} from '../services/search.service';

@Component({
  selector: 'app-starwars-search',
  templateUrl: './starwars-search.component.html',
  styleUrls: ['./starwars-search.component.css']
})
export class StarwarsSearchComponent implements OnInit {


  whichCategory:String
  frmId:number = 1
  choices:Object[]=[{cat:'people'}, {cat:'planets'}, {cat:'vehicles'}, {cat:'species'}, {cat: 'starships'}]
  model
  listHidden=true


  constructor(private service:SearchService) { }

  ngOnInit() {
  }

  //Method that is called when button is pressed
  handleClick(){
    this.service.getUserSpecifiedData(this.frmId, this.whichCategory)
      .subscribe( (result) => {this.model =result
        this.listHidden=false})
    }

  }


