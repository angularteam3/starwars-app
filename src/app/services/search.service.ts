import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http:HttpClient) { }

  url = "https://swapi.co/api/"

  //Gets the request from the user and retrives the data
  getUserSpecifiedData(category, id){
    return this.http.get(`${this.url}${id}/${category}`)
    .pipe(
      catchError(this.handleError<Object[]>('getParamData', []))
    );
  }




  //Handerler for errors

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return throwError('Something bad happened');
    };
  }

}
